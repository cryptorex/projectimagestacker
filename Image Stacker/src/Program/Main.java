package Program;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		/*
		 * Works on the assumtion that all the "stack" files use same coding so the
		 * header for image .ppm files are the same for one picture. For example we
		 * use the image which has these stats as p3 600 346 for all stacks and
		 * output should use the same too.
		 * 
		 * You can give 2 types of arguments, if you only give the directory where the
		 * files are ( there can be only stackfiles in that specific folder ), then you 
		 * will get the result picture in source folder. If you give 2 arguments , you
		 * will get the result picture in that specific folder. 
		 * 
		 * NB! You only need to give the directory name for input and the program will
		 * do the rest. You might want to also rename the result , then give the
		 * specific result path.
		 * 
		 * Example of run configuration : 1) Pictures/wfc3_uvis result.ppm
		 * 								  2) Pictures/wfc3_uvis
		 */

		try {
			NoiseProcessor.process(args);
		} catch (IOException e) {
			throw new RuntimeException("something went wrong here." + e);
		}
		

	}

}
