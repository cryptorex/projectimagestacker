package Program;

import java.io.*;
import java.util.*;

public class NoiseProcessor {

	public static void process(String... filenames) throws IOException {
		StringBuilder sb = new StringBuilder(filenames[0]);
		String[] filenamePieces = filenames[0].split("/");
		String fileName = sb.append("/" + filenamePieces[filenamePieces.length -1]).toString();
		String outName = filenamePieces[filenamePieces.length-1] + ".ppm";
		if (filenames.length==2){
			outName = filenames[1];
			if (!outName.endsWith(".ppm")){
				outName = filenames[1] + filenamePieces[filenamePieces.length-1] + ".ppm";
			}
		}
		int stackCount = new File(filenames[0]).list().length; 

		BufferedReader br = new BufferedReader(new FileReader(new File(fileName+"_001.ppm")));
		Header header = readHeader(br);
		br.close();
		
		List<RGB[][]> fullRgbList = new ArrayList<RGB[][]>();
		for (int i = 1; i <= stackCount; i++) {
			String stackFileName = String.format("%s_%03d.ppm", fileName, i);
			fullRgbList.add(readOnePic(stackFileName,header));
		}
		System.out.println("All the pics have been read ...");
		RGB[][] processedArray = averageRgb(fullRgbList, stackCount);
		
		writePpmToFile(processedArray,outName,header);
		System.out.println("Programmi t�� on l�ppenud ...");;
		 
	}

	private static void writePpmToFile(RGB[][] processedArray, String outfile, Header header) {
		System.out.println("writer is starting to write...");
		try {	
			BufferedWriter bwout = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(outfile))));
			bwout.write(header.getHeaderAsString());
			for (int i = 0; i < processedArray.length; i++) {
				for (int j = 0; j < processedArray[i].length; j++) {
					bwout.write(processedArray[i][j].getPpmFormat());
					bwout.newLine();
				}
			}
			bwout.close();
			System.out.println("Program has finished working successsfully");
		} catch (IOException e) {
			throw new RuntimeException("WritePpmToFile method has failed!" + e);
		}
	}

	private static RGB[][] averageRgb(List<RGB[][]> listIn, int stackCount) {
		System.out.println("averageRgb meetod alustab t��d");
		RGB[][] result = new RGB[listIn.get(0).length][listIn.get(0)[0].length];
		for (int i = 0; i<result.length; i++){
			for (int j = 0; j<result[i].length; j++){
				RGB[] pixelRgbs = new RGB[stackCount];
				for (int k = 0; k < listIn.size(); k++) {
					pixelRgbs[k] = listIn.get(k)[i][j];
				}
				result[i][j] = averagePixelRgbs(pixelRgbs);
			}
		}
		System.out.println("AverageRgb meetod l�petab t�� ... Algab kirjutamine ... ");
		return result;
	}

	private static RGB[][] readOnePic(String fileName, Header header) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
		readThreeLines(br);
		RGB[][] onePicArray = new RGB[header.getHeight()][header.getWidth()];
		try {
			String line = "";
			for (int i = 0; i < header.getHeight(); i++) {
				for (int j = 0; j < header.getWidth(); j++) {
					line = br.readLine();
					String[] pieces = line.split(" ");
					onePicArray[i][j] = new RGB(Integer.parseInt(pieces[0]),
								Integer.parseInt(pieces[1]), Integer.parseInt(pieces[2]));
				}
			}
			br.close();
		} catch (IOException e) {
			throw new RuntimeException("Readonepic ioexception..." + e);
		}
		return onePicArray;
	}

	private static Header readHeader(BufferedReader br) {
		try {
			String header = br.readLine();
			String[] pieces = br.readLine().split(" ");
			int width = Integer.parseInt(pieces[0]);
			int height = Integer.parseInt(pieces[1]);
			int maxValue = Integer.parseInt(br.readLine());
			Header head = new Header(header, width, height, maxValue);
			return head;
		} catch (IOException e) {
			throw new RuntimeException("ReadHeader method failed ..." + e);
		}

	}

	private static RGB averagePixelRgbs(RGB[] rgbIn){
		int red = 0;
		int green = 0;
		int blue = 0;
		int len = rgbIn.length;
		for (int i = 0; i < rgbIn.length; i++) {
			red += rgbIn[i].getRed();
			green += rgbIn[i].getGreen();
			blue += rgbIn[i].getBlue();
		}
		return new RGB(red/len,green/len,blue/len);
	}
	
	private static void readThreeLines(BufferedReader br) {
		try {
			br.readLine();
			br.readLine();
			br.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
