package Program;

public class RGB {
	private int red;
	private int green;
	private int blue;
	
	public RGB(int r, int g, int b) {
		this.red=r;
		this.green = g;
		this.blue = b;
	}

	public int getRed() {
		return red;
	}

	public int getGreen() {
		return green;
	}

	public int getBlue() {
		return blue;
	}

	@Override
	public String toString() {
		return "RGB [red=" + red + ", green=" + green + ", blue=" + blue + "]";
	}
	
	public String getPpmFormat() {
		return "" + red + " "+ green + " " + blue;
	}
	
}
