package Program;


public class Header {
	private String type;
	private int width;
	private int height;
	private int maxValue;
	
	public Header(String type, int width, int height, int maxValue) {
		super();
		this.type = type;
		this.width = width;
		this.height = height;
		this.maxValue = maxValue;
	}
	
	
	public int getWidth() {
		return width;
	}


	public int getHeight() {
		return height;
	}
	
	public String getWidthString(){
		return Integer.toString(width);
	}

	public String getHeaderAsString() {
		return type + "\n" + Integer.toString(width) + " " + Integer.toString(height) + "\n" + 
					Integer.toString(maxValue) + "\n";
	}
	
	
	
}
